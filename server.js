const mysql = require('mysql');
const express = require('express');
var app = express();
const bodyparser = require('body-parser');

app.use(bodyparser.json());

var mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password:'nabin123',
    database:'skyniche',
    multipleStatements: true
});

mysqlConnection.connect((err) => {
    if (!err)
        console.log('DB connected.');
    else
        console.log('DB connection failed \n Error : ' + JSON.stringify(err, undefined, 2));
});

//Insert an employees
app.post('/employees', (req, res) => {
    let emp = req.body;
    var sql = "SET @eid = ?;SET @name = ?;SET @email = ?;SET @phone = ?;SET @designation = ?;SET @date_of_join = ?; \
    CALL employeeAddOrEdit(@eid,@name,@email, @phone,@designation, @date_of_join);";
    mysqlConnection.query(sql, [emp.eid,emp.name,emp.email, emp.phone,emp.designation, emp.date_of_join], (err, rows, fields) => {
        if (!err)
            rows.forEach(element => {
                if(element.constructor == Array)
                res.send('Inserted employee id : '+element[0].eid);
            });
        else
            console.log(err);
    })
});

//Get all employees
app.get('/employees', (req, res) => {
    mysqlConnection.query('SELECT * FROM employee', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Get an employees
app.get('/employees/:id', (req, res) => {
    mysqlConnection.query('SELECT * FROM employee WHERE eid = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});


//Update an employees
app.put('/employees', (req, res) => {
    let emp = req.body;
    var sql = " SET @eid = ?;SET @name = ?;SET @email = ?;SET @phone = ?;SET @designation = ?;SET @date_of_join = ?; \
    CALL employeeAddOrEdit(@eid,@name,@email, @phone,@designation, @date_of_join);";
    mysqlConnection.query(sql, [emp.eid,emp.name,emp.email, emp.phone,emp.designation, emp.date_of_join], (err, rows, fields) => {
        if (!err)
            res.send('Updated successfully');
        else
            console.log(err);
    })
});

//Delete an employees
app.delete('/employees/:id', (req, res) => {
    mysqlConnection.query('DELETE FROM employee WHERE eid = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send('Deleted successfully.');
        else
            console.log(err);
    })
});

app.listen(3000, () => 
console.log('Express server is runnig at port no : 3000'));
